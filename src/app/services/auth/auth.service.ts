import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public http: HttpClient) { }

  getMsg() {
    return this.http.get<JSON>(`${environment.baseUrl}login`)
  }

  // Register the user
  createUser(name:string, email:string, password:string) {
    let data = new FormData();
    data.append('name', name)
    data.append('email', email)
    data.append('password', password)
    return this.http.post(`${environment.baseUrl}signup`,data)
  }
}
