import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { FeedsPage } from './feeds/feeds.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    // children: [
    //   {
    //     path: 'share',
    //     component: ShareComponent
    //  },
    // ]
  },
  {
    path: 'comments',
    loadChildren: () => import('./feeds/feeds.module').then( m => m.FeedsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
