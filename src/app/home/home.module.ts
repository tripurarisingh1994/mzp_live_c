import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule} from './home-routing.module'
import { FeedsPage } from './feeds/feeds.page';
import { PostsComponent } from './posts/posts.component';
import { HeaderComponent } from '../header/header.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HomePageRoutingModule,
    // RouterModule.forChild([{ path: '', component: HomePage }])
  ],
  declarations: [HomePage, HeaderComponent, FeedsPage, PostsComponent]
})
export class HomePageModule {}
