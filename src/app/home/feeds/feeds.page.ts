import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.page.html',
  styleUrls: ['./feeds.page.scss'],
})
export class FeedsPage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async share() {
    const alert = await this.alertController.create({
      header: 'Share Via',
      cssClass: 'share',
      buttons: [
        {
            text: '',
            cssClass: 'w'
        },

        {
          text: '',
          cssClass: 'f'
        },

        {
          text: '',
          cssClass: 't'
        },
      ]
    });

    await alert.present();
  }
  

}
