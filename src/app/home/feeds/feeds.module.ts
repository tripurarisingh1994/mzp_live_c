import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { CommentsPage } from './comments/comments.page';
import { FeedsPageRoutingModule } from './feeds-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedsPageRoutingModule
  ],
  declarations: [CommentsPage]
})
export class FeedsPageModule {}
